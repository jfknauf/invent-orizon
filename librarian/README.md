# librarian

A Clojure web application designed to support easier stocktaking of JFK's library.

* Checkout your inventory list
* Run the app
* Pick up a mobile device and connect through the local WiFi
* Confirm all books you want to confirm
* Check in the resulting updated inventory list

## Usage

    lein ring server-headless

This starts a server on port 3000 by default. Make sure your VM binds the port in a way to access it from the local WiFi using a mobile device.

## License

Copyright © 2019 Johannes F. Knauf

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
