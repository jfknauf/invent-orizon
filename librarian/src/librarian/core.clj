(ns librarian.core
  (:require [ring.adapter.jetty :as jetty]
            [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [librarian.controller :as controller]))

(defroutes app-routes
  (GET "/" [] (controller/index))
  (POST "/inventorize" req (controller/post req))
  (route/resources "/")
  (route/not-found "Page not found"))

(def handler
  (handler/site app-routes))

(defn -main []
  (jetty/run-jetty handler {:port 3000}))

