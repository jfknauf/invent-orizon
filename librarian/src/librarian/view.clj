(ns librarian.view
  (:use hiccup.page hiccup.element)
  (:require [ring.util.response :as response]))

(def app-host "http://192.168.178.29:3000")

(defn book-input-li [book]
  [:tr
   [:td (str (:id book))]
   [:td [:dl
         [:dt "Title"]
         [:dd (str (:title book))]
         [:dt "Last stockkeeping"]
         [:dd (str (get-in book [:last_stocktaking :date]))]
         [:dt "Location"]
         [:dd (str (get-in book [:last_stocktaking :location]))]]]
   [:td
    [:input {:type "checkbox"
             :name "confirmed"
             :value (:id book)}]]])

(defn index-page [books]
  (html5
   [:html
    [:head]
    [:body
     [:form {:action (str app-host "/inventorize") :method "post"}
      [:table
       [:thead
        [:th "ID"]
        [:th "Details"]
        [:th "Location confirmed"]]
       [:tbody
        (->> books
             (sort-by #(get-in % [:last_stocktaking :date]))
             (map book-input-li))]]
      [:button {:type "submit"} "Submit"]]]]))


(defn post-page []
  (response/redirect (str app-host "/") :see-other))
