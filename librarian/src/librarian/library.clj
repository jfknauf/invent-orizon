(ns librarian.library
  (:require [yaml.core :as yaml]))

(defn confirm-location [book date]
  (assoc-in book [:last_stocktaking :date] date))

(defn confirm-locations [books date book-ids]
  (let [book-ids (set book-ids)]
    (map (fn [book]
           (if (contains? book-ids (:id book))
             (confirm-location book date)
             book))
         books)))

(defn find-by-id [books id]
  (let [has-id? #(= id (:id %1))]
    (first (filter has-id? books))))
