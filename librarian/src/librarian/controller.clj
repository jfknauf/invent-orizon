(ns librarian.controller
  (:require [librarian.library :as library]
            [librarian.view :as view]
            [yaml.core :as yaml]))

(def library-file "library_jfk.yml")

(defn get-books-db []
  (yaml/parse-string (slurp library-file)))

(defn store-books-db [books]
  (spit library-file (yaml/generate-string books :dumper-options {:flow-style :block :scalar-style :double-quoted :split-lines false})))

(defn current-date []
  (-> "Europe/Berlin"
      (java.time.ZoneId/of)
      (java.time.ZonedDateTime/now)
      (.format java.time.format.DateTimeFormatter/ISO_LOCAL_DATE)))

(defn index []
  (let [books (get-books-db)]
    (view/index-page books)))


(defn post [{params :form-params}]
  (do
    (let [confirmed-books (get params "confirmed")
          books (get-books-db)
          
          updated-books (library/confirm-locations books (current-date) confirmed-books)]
      (store-books-db updated-books))
    (view/post-page)))
