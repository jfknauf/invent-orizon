(defproject librarian "0.1.0-SNAPSHOT"
  :description "Librarian is a web application to manage books"
  :url "https://metamorphant.de/books"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [ring "1.7.1"]
                 [compojure "1.6.1"]
                 [hiccup "1.0.5"]
                 [io.forward/yaml "1.0.9"]]
  :plugins [[lein-ring "0.12.5"]]
  :ring {:handler librarian.core/handler}
  :main librarian.core)
