(ns librarian.library-test
  (:require [clojure.test :refer :all]
            [librarian.library :refer :all]))

(def demo-books
  [{:id "0001" :title "Big Adventures" :last_stocktaking {:date "2019-01-02"}}
   {:id "0002" :title "Little Adventures" :last_stocktaking {:date "2019-01-01"}}
   {:id "0003" :title "Other Adventures" :last_stocktaking {:date "2019-01-03"}}])

(deftest library-test
  (testing "Stocktaking"
    (let [demo-book {:id "0002" :last_stocktaking {:date "2019-01-01"}}
          result-book (confirm-location demo-book "2019-05-09")
          stocktaking-date (get-in result-book [:last_stocktaking :date])]
      (is (= "2019-05-09" stocktaking-date)))
    (let [result-db (confirm-locations demo-books "2019-05-09" ["0001" "0002"])
          stocktaking-dates (map #(get-in % [:last_stocktaking :date]) result-db)]
      (is (= ["2019-05-09" "2019-05-09" "2019-01-03"] stocktaking-dates))))
  (testing "Finding by id"
    (let [result-entry (find-by-id demo-books "0002")]
          (is (= "Little Adventures" (:title result-entry))))))
