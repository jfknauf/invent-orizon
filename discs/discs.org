* Kastaplast Berg
** K1 Line
** Putt & Approach
** 1 1    0   2
** 174g
* DiscMania Link
** Evolution
** Putter
** 2 3    0   1
** 175g
* Dynamic Discs Judge
** Prime
** Putter
** 2 4    0 0.5
** 176g
* MVP Ohm
** Neutron Plastic
** Putt/Approach
** 2 5    0   1
** 175g
* MVP Atom
** Electron Firm plastic
** putt / approach
** 3 3 -0.5   0
** 174g
* DiscMania Logic
** Evolution Hard Exo
** Putter
** 3 3    0   1
** 175g
* Kastaplast Reko
** K3 Line
** Putt & Approach
** 3 3    0   1
** 174g
* Axiom Discs Envy
** Proton Plastic
** Putt & Approach
** 3 3    0   2
** 174g
* Axiom Discs Soft Envy
** Neutron Plastic
** Putt & Approach
** 3 3    0   2
** 173g
* Innova Mako3
** Champion
** Midrange
** 5 5    0   0
** 178g
* DiscMania Origin
** Evolution Neo
** Midrange
** 5 5   -1   1
** 179g
* MVP Uplink
** Neutron Plastic
** Midrange
** 5 5   -3 0.5
** 175g
* Discraft Comet
** Z-Line
** Midrange
** 4 5   -2   1 0.0
** 177g
* Discraft Buzzz
** Z-Line
** Midrange
** 5 4   -1   1 0.5
** 175g
* Kastaplast Svea
** K1 Line
** Midrange
** 5 6   -1   0
** 180g
* Axiom Discs Hex
** Neutron Plastic
** Midrange
** 5 5   -1   1
** 175g
* MVP Signal
** Neutron Plastic
** Fairway Driver
** 6 5   -3   1
** 168g
* Streamline Discs Drift
** Neutron Plastic
** Fairway Driver
** 7 5   -2   1
** 175g
* MVP Volt
** Cosmic Neutron Plastic
** Fairway Driver
** 8 5 -0.5   2
** 176g
* Latitude 64 Diamond
** Opto
** Easy-to-Use Driver
** 8 6   -3   1
** 159g
* MVP Lab Second
** Unknown
** Fairway Driver
** 9 5 -0.5   2
*** (selbst geschätzt)
** 174g
* MVP Impulse
** Neutron Plastic
** Distance Driver
** 9 5   -3   1
** 164g
* Discraft Heat
** ESP
** Distance Driver
** 9 6   -3   1 0.0
** 173g
* Axiom Discs Insanity
** Proton Plastic
** Distance Driver
** 9 5 -2 1.5
** 174g (laut Thrownatur)
*** 29.
*** 173
* DiscMania Instinct
** Evolution Neo
** Fairway Driver
** 7 5 0 2
** 176g (laut Thrownatur)
* DiscMania Tactic
** Evolution Hard Exo
** Approach Disc
** 4 2 0 3
** 174g (laut Thrownatur)
* DiscMania Stratosphere
** Horizon
** DD1
** 11 5 -1.0 2.0 (laut Thrownatur Website)
** 174g (laut Thrownatur)
** Special Edition
* MVP Matrix
** Neutron Plastic
** Midrange Driver
** 5 4 -1.0 2
** 179g (laut Thrownatur)
*** 178
* Streamline Discs Stabilizer
** Neutron Plastic
** Putt & Approach
** 3 3.5 0 3
** 168g (laut Thrownatur)
